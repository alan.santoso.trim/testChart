//
//  ViewController.swift
//  testChart
//
//  Created by Alan Santoso on 26/05/23.
//

import UIKit
import SafariServices
import WebKit
import Lottie

class ViewController: UIViewController, WKNavigationDelegate, WKScriptMessageHandler {
    
    var webView: WKWebView!
    var timer: Timer?
    
    let timestamp = 1685093362
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let configuration = WKWebViewConfiguration()
        let userContentController = WKUserContentController()
        userContentController.add(self, name: "myScriptMessageHandler")
        configuration.userContentController = userContentController
        
        
        webView = WKWebView(frame: view.frame, configuration: configuration)
        
        view.addSubview(webView)
        webView.translatesAutoresizingMaskIntoConstraints = false
        webView.navigationDelegate = self
        
        
        NSLayoutConstraint.activate([
            webView.topAnchor.constraint(equalTo: view.topAnchor),
            webView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            webView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            webView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
        
        
        
        let urlString = "http://dev-chartview-lite.trimegah.id/?chart=area&symbol=AALI&resolution=1M&from=1590481169&to=\(timestamp)"
        
        
        if let url = URL(string: urlString) {
            var request = URLRequest(url: url)
            request.setValue("application/json, text/plain, */*", forHTTPHeaderField: "Accept")
            request.setValue("gzip, deflate", forHTTPHeaderField: "Accept-Encoding")
            request.setValue("en-US,en;q=0.5", forHTTPHeaderField: "Accept-Language")
            request.setValue("Bearer eyJhbGciOiJFUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJhTU9ra3kzMnNrSjZ6d0E0NTM0ZUVVNENmeUJVamRrTjBDOERDNFdhbEs0In0.eyJleHAiOjE2ODU0NDYwNTQsImlhdCI6MTY4NTQxNzI1NCwianRpIjoiYWZiNzgyMGYtY2JiYS00NzQzLWI1YjMtMjlhOTRlODgwMWIwIiwiaXNzIjoiaHR0cDovL2Rldi1hdXRoLW1hbmFnZXIudHJpbWVnYWguaWQvYXV0aC9yZWFsbXMvcm9yb2pvbmdyYW5nIiwiYXVkIjoiYWNjb3VudCIsInN1YiI6Ijc4MzQxNTI5LWM1ZDctNDI1OC04YjlkLTM2NDFlMzg4NTk3YyIsInR5cCI6IkJlYXJlciIsImF6cCI6IlRSSU1BIiwic2Vzc2lvbl9zdGF0ZSI6ImU1NWM3ODM1LTFiMGItNDA3ZS1iMTk0LWM1M2FhYjExYjZhMyIsImFjciI6IjEiLCJyZWFsbV9hY2Nlc3MiOnsicm9sZXMiOlsiZGVmYXVsdC1yb2xlcy1yb3Jvam9uZ3JhbmciLCJvZmZsaW5lX2FjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJwcm9maWxlIGVtYWlsIiwic2lkIjoiZTU1Yzc4MzUtMWIwYi00MDdlLWIxOTQtYzUzYWFiMTFiNmEzIiwidXNlcl9lbWFpbCI6IlBNT0BUUklNRUdBSC5DT00iLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsIm5hbWUiOiJwbW8gcG1vIiwicmVnX3N0ZXAiOiIyIiwidXNlcl9waG9uZSI6IjA4MTM4MjMxODA5MyIsImxpc3RfYWNjIjpbIktSSVM5MTNEIiwiS1JJUzkxM0UiLCJLUklTOTEzRiIsIktSSVM5MTNMIiwiS1JJUzkxM1QiXSwidXNlcl90cmltYSI6InBtbyIsInByZWZlcnJlZF91c2VybmFtZSI6InBtbyIsImdpdmVuX25hbWUiOiJwbW8iLCJmYW1pbHlfbmFtZSI6InBtbyIsInVzZXJfY2lmIjoiQ0lGLUtSSVM5MTNUIiwiZW1haWwiOiJwbW9AdHJpbWVnYWguY29tIn0.0Fia4x1OEtZPuzb0y2CdHFJ8oXqhatNoLInt4fidYKfqUONCwSbDkbigp7tnAJzDmlZbwoKgNwbqIude6lQ2ZA", forHTTPHeaderField: "Authorization")
            request.setValue("keep-alive", forHTTPHeaderField: "Connection")
            request.setValue("dev-chartview-lite.trimegah.id", forHTTPHeaderField: "Host")
            request.setValue("http://dev-chartview-lite.trimegah.id/?chart=area&symbol=AALI&resolution=1M&from=1590481169&to=1685097738", forHTTPHeaderField: "Referer")
            request.setValue("1", forHTTPHeaderField: "Sec-GPC")
            request.setValue("Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1", forHTTPHeaderField: "User-Agent")
            
            
            
            
            webView.load(request)
            // Start the timer to reload the web view every 30 seconds
            //        timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(reloadWebView), userInfo: nil, repeats: true)
            //
        }
    }
    
    
    @objc func reloadWebView() {
        webView.reload()
    }
    
    
    
    
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        if let newURL = webView.url {
            print("New URL: \(newURL)")
            // Do further processing with the new URL if needed
        }
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        // Inject JavaScript to print the document title
        webView.evaluateJavaScriptWithResult("document.title") { (result, error) in
            if let title = result as? String {
                print("Document Title: \(title)")
            }
        }
        
        // Inject JavaScript to print the HTML content
        webView.evaluateJavaScriptWithResult("document.documentElement.outerHTML") { (result, error) in
            if let html = result as? String {
                print("HTML Content: \(html)")
            }
        }
        
        // Inject JavaScript to print a specific element by its ID
        let elementId = "myElementId"
        let jsCode = "document.getElementById('\(elementId)').outerHTML"
        webView.evaluateJavaScriptWithResult(jsCode) { (result, error) in
            if let elementHTML = result as? String {
                print("Element with ID '\(elementId)': \(elementHTML)")
            }
        }
    }
    
    func getCurrentUnixTimestamp() -> Int {
        let currentTime = Date().timeIntervalSince1970
        return Int(currentTime)
    }
    
    func logJavaScriptOutput() {
        let script = "console.log('Hello from JavaScript')"
        
        webView.evaluateJavaScript(script) { (result, error) in
            if let error = error {
                print("JavaScript error: \(error.localizedDescription)")
            } else if let result = result {
                print("JavaScript result: \(result)")
            }
        }
    }
    
    // MARK: - WKNavigationDelegate
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        print("Failed navigation with error: \(error.localizedDescription)")
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        print("Failed navigation with error: \(error.localizedDescription)")
    }
    
    // MARK: - WKScriptMessageHandler
    
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        if message.name == "myScriptMessageHandler" {
            print("Received JavaScript message: \(message.body)")
        }
    }
    
    func webViewWebContentProcessDidTerminate(_ webView: WKWebView) {
        print("termintaed")
    }
    
    // Called when the web view receives the response for the navigation request
    private func webView(_ webView: WKWebView, didReceive response: WKNavigationResponse, completionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
        // Get the response data
        if let httpResponse = response.response as? HTTPURLResponse {
            let statusCode = httpResponse.statusCode
            let headers = httpResponse.allHeaderFields
            
            // Process the response data as needed
            // ...
        }
        
        completionHandler(.allow)
    }
    
    
}

extension WKWebView {
    func evaluateJavaScriptWithResult(_ javaScriptString: String, completionHandler: @escaping ((Any?, Error?) -> Void)) {
        evaluateJavaScript(javaScriptString) { (result, error) in
            completionHandler(result, error)
        }
    }
}
